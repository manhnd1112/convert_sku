import os
import sys
import pandas as pd
from pandas import read_excel
import re
from custom_config import CustomConfigParser
import traceback
import math


def map_owner_code(origin_owner_code, config_parser=None, config_filepath='./shoes_sku.ini'):
    if config_parser is None:
        config_parser = CustomConfigParser(config_filepath)
    return config_parser.get_dict('DEFAULT', 'MAP_OWNER_CODE')[origin_owner_code]

def map_gender(origin_gender, config_parser=None, config_filepath='./shoes_sku.ini'):
    if config_parser is None:
        config_parser = CustomConfigParser(config_filepath)
    return config_parser.get_dict('DEFAULT', 'MAP_GENDER')[origin_gender]

def map_color(origin_color, config_parser=None, config_filepath='./shoes_sku.ini'):
    if config_parser is None:
        config_parser = CustomConfigParser(config_filepath)
    return config_parser.get_dict('DEFAULT', 'MAP_COLOR')[origin_color]

def map_men_size_US2CHINA_EU(origin_us_size, config_parser=None, config_filepath='./shoes_sku.ini'):
    if config_parser is None:
        config_parser = CustomConfigParser(config_filepath)
    china_us_size = config_parser.get_dict('DEFAULT', 'MAP_MEN_SIZE_US2CHINA_US')[origin_us_size]
    china_eu_size = config_parser.get_dict('DEFAULT', 'MAP_SIZE_CHINA_US2CHINA_EU')[china_us_size]
    return china_eu_size

def map_women_size_US2CHINA_EU(origin_us_size, config_parser=None, config_filepath='./shoes_sku.ini'):
    if config_parser is None:
        config_parser = CustomConfigParser(config_filepath)
    china_us_size = config_parser.get_dict('DEFAULT', 'MAP_WOMEN_SIZE_US2CHINA_US')[origin_us_size]
    china_eu_size = config_parser.get_dict('DEFAULT', 'MAP_SIZE_CHINA_US2CHINA_EU')[china_us_size]
    return china_eu_size
    
def convert_sku(origin_sku, config_parser):
    try:
        sku_pat = re.compile(config_parser['DEFAULT']['SKU_PAT'])
        for owner_code, order_id, gender, color, us_size in sku_pat.findall(origin_sku):
            #print('{}-{}-{}-{}-{}'.format(owner_code, order_id, gender, color, us_size))
            maped_owner_code = map_owner_code(owner_code, config_parser=config_parser)
            maped_gender = map_gender(gender, config_parser=config_parser)
            maped_color = map_color(color, config_parser=config_parser)
            if gender == 'M':
                mapped_china_eu_size = map_men_size_US2CHINA_EU(us_size, config_parser=config_parser)
            else:
                mapped_china_eu_size = map_women_size_US2CHINA_EU(us_size, config_parser=config_parser)            
            print("Origin SKU: {}".format(origin_sku))

            converted_sku = '{}{}{}{}{}'.format(maped_owner_code, order_id, maped_gender, maped_color, mapped_china_eu_size)
            print('Converted SKU: {}'.format(converted_sku))
            return converted_sku 
    except KeyError:
        print(origin_sku)
        traceback.print_exc()
        return origin_sku
    except TypeError: 
        print(origin_sku)
        traceback.print_exc()
        return origin_sku

def validate_phone(phone_number, config_parser=None, config_filepath='./shoes_sku.ini'):
    try:
        if config_parser is None:
            config_parser = CustomConfigParser(config_filepath)
        if math.isnan(phone_number):
            return config_parser.get('DEFAULT', 'DEFAULT_PHONE_NUMBER')
        return phone_number
    except TypeError:
        return phone_number


def read_xlsx(xlsx_fpath, sheet_name):
    config_parser = CustomConfigParser('./shoes_sku.ini')
    df = read_excel(xlsx_fpath, sheet_name = sheet_name)
    print(df["SKU"])
    df['SKU'] = df.apply (lambda row: convert_sku(row['SKU'], config_parser), axis=1)
    df['Phone (Billing)'] = df.apply (lambda row: validate_phone(row['Phone (Billing)'], config_parser), axis=1)
    head, tail = os.path.split(xlsx_fpath)
    df.to_excel('{}/{}_converted_sku.xlsx'.format(head, tail.split('.')[0]), index=False)
    
if __name__ == "__main__":
    # convert_sku(origin_sku="HMG1005MBlackUS11")
    # convert_sku(origin_sku="HMG1005WBlackUS11")
    read_xlsx(str(sys.argv[1]), 'Orders')