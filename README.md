# Covert SKU

## Clone project
```bash
git clone https://manhnd1112@bitbucket.org/manhnd1112/convert_sku.git`
```

## Convert SKU
```bash
cd convert_sku
python convert_sku.py <path-to-oder-file-xlsx>
```