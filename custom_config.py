import configparser
from configparser import ConfigParser
import ast

class CustomConfigParser(ConfigParser):
    def __init__(self, config_filepath):
        super(CustomConfigParser, self).__init__()
        self.read_file(open(config_filepath))

    def get_list(self, section, option):
        return ast.literal_eval(self.get(section, option))
    
    def get_dict(self, section, option):
        return ast.literal_eval(self.get(section, option))
